import  { getUsers } from './api/getUsers.js'
import  { getPosts } from './api/getPosts.js'
import  { Posts } from './components/Posts.jsx'
import  { Filter } from './components/Filter.jsx'
import  { App } from './components/App.jsx'
import  { createStore, applyMiddleware } from 'redux'
import createLogger from 'redux-logger'
import  {reducer} from './reducer.js'
import React from 'react'
import ReactDOM from 'react-dom' 

var fullPosts = Promise.all([getPosts(), getUsers()])
.then(([posts, users]) => 
  posts.map(post => ({
    key: post.id,
    userId: post.userId,
    name: users[post.userId],
    text: post.body,
    id: post.id
  }))
)

const store = createStore(reducer, applyMiddleware(createLogger()))
// const filter = store.filter
// const posts = store.posts
const setListeners = store.subscribe
const dispatch = store.dispatch

function setFilter(newFilters)
{
  dispatch (
    {
      type: 'SET_FILTER',
      payload: newFilters
    }
  )
}

function addPosts(posts){
  dispatch (
  {
    type:'ADD_POSTS',
    payload: posts
  }
  )
}

function render(){
  const state = store.getState()
  console.log(state)
  const posts = state.posts
  const filter = state.filter
  ReactDOM.render
  (
    <App 
    posts={posts} 
    filter={filter} 
    setFilter={setFilter} />, 

    document.getElementById('yo')
  )
}

setListeners(render)

fullPosts.then(posts => {
  addPosts(posts)
})
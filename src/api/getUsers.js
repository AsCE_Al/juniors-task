export function getUsers() {
    var users = fetch('//dev.onlinepbx.ru/users.php')
    .then(response => response.json())
    .then(response => response.reduce(
    (users, user) => Object.assign({}, users, {
    [user.id]: user.name
    }),
  {}
  ))
    return users;
}
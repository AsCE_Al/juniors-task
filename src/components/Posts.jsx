import  { Post } from './Post.jsx'
import React from 'react'

export function Posts(posts) {
  console.log(posts);
  if( posts.length == 0 || !posts ) {
    return (<div>
        Nothing Found
      </div>)
  }
  return (<div>
    {posts.map(post => <Post key={post.id} {...post} />)
    }
  </div>)
}

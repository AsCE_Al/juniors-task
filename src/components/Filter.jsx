export function filter(posts,filter) {
  if (!filter || filter <= 0) {
    return posts
  } else {
    return (posts.filter(({ userId }) => userId == filter));
  }
}

export default filter

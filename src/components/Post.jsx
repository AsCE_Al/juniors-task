import React from 'react'

export function Post({ id, name, text }){
  return (
    <div>
      <p>
        <small>{id}</small>
        {' '}
        {name}
      </p>
      <p>
        {text}
      </p>
    </div>
  )
}
import React from 'react'
import  { filter as Filter } from './Filter.jsx'

export class App extends React.Component{
    render(){
        const posts = this.props.posts
        const filter = this.props.filter
        const setFilter = this.props.setFilter
        const filteredPosts = Filter(posts, filter)
        return (<div>
            <div>
            <input min='0' type="number" onChange={(e)=>setFilter(e.target.value)}/>
            </div>
            <div>
            {filteredPosts.map(posts => <div key={posts.id}> <p>{posts.id} {" "} {posts.name}</p><p>{posts.text}</p></div>)}
            </div>
        </div>
        );
    }
}
